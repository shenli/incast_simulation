from server import Server
from simulator import Simulator
from event import Event
from mem_server import MemServer

from sets import Set
import random

class WebServer(Server):
  
  BANDWIDTH = 2e8 / 8.0
  OBJ_NUM = 350
  PROCESS_LATENCY = 0

  service_ips = None

  bw = 0
  out_wait = 0
  
  req_id = 0
  req_2_ports = None
  ports_2_req = None
  
  req_created_time = None
  delays = None

  def __init__(self, id, ip, sim, RTO = 2e5, \
               OBJ_NUM = 350, proto = 'TCP'):
    Server.__init__(self, id, ip, sim, RTO, proto)
    self.OBJ_NUM = OBJ_NUM

    self.service_ips = Set()
    self.req_id = 0

    self.req_2_ports = {}
    self.ports_2_req = {}

    self.out_wait = 0
    self.bw = self.BANDWIDTH

    self.req_created_time = {}
    self.delays = []

  def get_delays(self):
    return self.delays

  def get_max_bw(self):
    return self.BANDWIDTH

  def register_service(self, ip):
    self.service_ips.add(ip)

  def _random_obj_num(self):
    return self.OBJ_NUM

  def _random_req_size(self):
    return 100

  def request_send(self, t):
    self.req_id += 1
    """
    generate a random number of memcached read requests
    NOTE: for now, we use a fixed number of 350 read requests
    """  
    
    self.req_created_time[self.req_id] = t
    obj_num = self._random_obj_num()

    dsts = random.sample(self.service_ips, obj_num)
    
    if len(dsts) > 0:
      self.req_2_ports[self.req_id] = Set()
      for dst in dsts:
        """tcp makes sure the payload arrives at dst"""
        tcp = self.create_proto(self._get_port(), self.ip, dst, \
                                self.response_recv, self.nic_buff, \
                                self.sim, self.RTO)
        tcp_port = tcp.get_port()
        self.conn_pool[tcp_port] = tcp
        self.req_2_ports[self.req_id].add(tcp_port)
        self.ports_2_req[tcp_port] = self.req_id
        tcp.send(t, self._random_req_size())
      


  def response_recv(self, t, data):
    (tcp, data_size) = data
    if data_size > 0:
      t += self.PROCESS_LATENCY
      tcp.close(t)

    else:
      req_id = self.ports_2_req[-data_size]
      self.req_2_ports[req_id].remove(-data_size)

      if len(self.req_2_ports[req_id]) <= 0:
        self.req_2_ports.pop(req_id)
        #print("fulfilled request %d"%(req_id))

        self.delays.append(t - self.req_created_time[req_id])
        self.req_created_time.pop(req_id)
      

  def get_bw_util(self):
    return self.get_bw_recv() / float(self.bw)

  """
    recv and send are pkt level apis. application level should
    call TCP apis
  """
  def nic_recv(self, t, pkt):
    #print 'recv', t, '\t', pkt
    """ 
      only web_server initializes requests, so pkt.dst_port should never
      be negative
    """
    assert pkt.dst_port >= 0
    self._stat_nic_recv(t, pkt)

    t += self.OS_LATENCY
    tcp = self.conn_pool[pkt.dst_port]
    event = Event(time = t, callback = tcp.recv, data = pkt)
    self.event_queue.put((t, event))

  def nic_buff(self, t, pkt):
    if not "SYN" == pkt.action:
      self.pkts_buff += 1
    transmit_latency = 1e6 * pkt.size / self.bw
    self.out_wait = max(t, self.out_wait) + transmit_latency

    t = self.out_wait + self.OS_LATENCY
    event = Event(time = t, callback = self.nic_send, data = pkt)
    self.event_queue.put((t, event))

  def nic_send(self, t, pkt):
    pkt.ack_weight = max(0, 1 - self.get_bw_util())
    if self.sim.debug:
      print t, '\t', pkt
    self.sim.stat['record_all'](t, pkt)
    if not "SYN" == pkt.action:
      self.pkts_sent += 1
    self.neighbor.nic_recv(t, pkt)

  def get_fib(self, except_ports = []):
    return [self.ip]

  def insert_fib(self, port, ips):
    pass


  def connect(self, node):
    self.neighbor = node
    self.bw = min(self.BANDWIDTH, node.bw)


def main():
  sim = Simulator()
  web = WebServer(0, '1.1.1.1', sim.get_event_queue())
  mem = MemServer(0, '2.2.2.2', sim.get_event_queue())

  web.connect(mem)
  mem.connect(web)
  web.register_service(mem.ip)

  web.OBJ_NUM = 1
  web.request_send(0)

  sim.simulate()

if __name__=="__main__":
  main()
