from node import Node

class Mock_Server(Node):

  ip = None

  def __init__(self, id, ip):
    Node.__init__(self, id);
    self.ip = ip

  def recv(self, t, pkt):
    print("recieving packet at time %d on node %s: %s"%(t, self.ip, pkt))

  def get_fib(self, except_ports = []):
    return [self.ip]

  def insert_fib(self, port, ips):
    pass
