from simulator import Simulator
from mem_server import MemServer
from web_server import WebServer
from switch import Switch
from node import Node
from event import Event


def main():
  sim = Simulator()
  switch1 = Switch(0, sim.get_event_queue())
  switch2 = Switch(1, sim.get_event_queue(), port_num = 500)

  web = WebServer(100, '1.1.1.1', sim.get_event_queue())
  #mem = MemServer(101, '2.2.2.2', sim.get_event_queue())

  switch1.connect(0, web, -1)
  web.connect(switch1)

  for i in range(2, 400):
    ip = ".".join(['%d'%(i) for x in range(4)])
    mem = MemServer(100 + i, ip, sim.get_event_queue())
    switch2.connect(i, mem, -1)
    mem.connect(switch2)
    web.register_service(mem.ip)

  switch1.connect(1, switch2, 1)
  switch2.connect(1, switch1, 1)

  
  web.request_send(0)

  sim.simulate()


if __name__=="__main__":
  main()
