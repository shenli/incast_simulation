from Queue import Queue, PriorityQueue
from tcp import TCP
from lp_packet import LpPacket
import math
import random

from event import Event

class LPTCP(TCP):

  wait_callback = None
  bw_callback = None

  prev_pkt_time = 0

  def __init__(self, src_port, src_ip, dst_ip, recv_callback, \
               send_callback, sim, wait_callback, \
               bw_callback, RTO = 2e5):
    TCP.__init__(self, src_port, src_ip, dst_ip, recv_callback, \
                 send_callback, sim, RTO)
    
    self.wait_callback = wait_callback    
    self.bw_callback = bw_callback
    self.prev_pkt_time = 0
    self.stat['wait'] = 0
    self.ack_weight_cnt = 0.0

    self.INIT_WINDOW_SIZE = 1
    self.cur_window_size = 1

  def create_packet(self, t, src_port, dst_port = -1, size = 1500, \
                    src_ip = '0.0.0.0', dst_ip = '1.1.1.1', \
                    seq_no = 0, action = None, proto = 'LPTCP'):
    pkt = LpPacket(t, src_port, dst_port = dst_port, size = size, \
                  src_ip = src_ip, dst_ip = dst_ip, \
                  seq_no = seq_no, action = action, proto = 'LPTCP')
    return pkt

  def _do_send_callback(self, t, data):
    (pkt, resend) = data
    pkt.lp_delay = self.wait_callback()
    pkt.ack_weight = 1 - self.bw_callback();
    pkt.created_time = t

    self.send_callback(t, pkt)
    if resend:
      self._schedule_resend(t)
      if not self.is_resending:
        self._window_append(pkt)

  """wraps the real callback to insert the lp_delay field"""
  def _send_callback(self, t, data):
    (pkt, resend) = data

    tt = 0
    if "DAT" == pkt.action or "END" == pkt.action or \
       "FIN" == pkt.action:
      if pkt.resend:
        tt = max(self.prev_pkt_time + 1, t)
      else:
        tt = max(self.prev_pkt_time + 1, t + self.stat['wait'])

      self.prev_pkt_time = tt

    if tt > t:
      event = Event(time = tt, callback = self._do_send_callback, \
                    data = data)
      self.event_queue.put((event.time, event))
    else:
      self._do_send_callback(t, data)

  def _stat_packet(self, t, pkt):
    self.stat['wait'] = pkt.lp_delay

  def _confirm_one_packet(self, pkt):
    cpkt = self.window_pkts.get()
    #print "confirm: ", cpkt
    if self.is_resending:
      self.resend_cursor -= 1

    if pkt.ack_weight > 0.3 and random.random() < (pkt.ack_weight - 0.3) / 0.7:
      if self.cur_window_size < self.MAX_WINDOW_SIZE:
        self.cur_window_size += 1

    self.remain_window_size += 2
    self.remain_window_size = min(self.remain_window_size,
                                  self.cur_window_size)

