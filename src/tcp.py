from Queue import Queue, PriorityQueue
from packet import Packet
import math

from event import Event

class TCP:

  MAX_RTO = 1e6
  INIT_RTO = 0
  HEADER_SIZE = 20
  MTU_SIZE = 1500
  INIT_WINDOW_SIZE = 2
  MIN_WINDOW_SIZE = 1
  MAX_WINDOW_SIZE = 50
  MAX_RETRY = 3

  ACK_TRIGGER_MAX = MIN_WINDOW_SIZE

  EPS = 1
  RTO = 10000
  TIMEOUT = 300 * 1e6
  
  src_port = -1
  dst_port = -1
  src_ip = None
  dst_ip = None
  recv_callback = None
  send_callback = None
  
  remain_pkts = None
  window_pkts = None
  cur_window_size = 0
  remain_window_size = 0
  #seq_no = 0 
  send_seq_no = -1
  recv_seq_no = -1

  is_resending = False
  resend_cursor = 0

  data_buf_size = 0
  ack_trigger_cnt = 0

  is_syned = False
  scheduled_resend = False
  scheduled_timeout = False
  is_closed = False
  fin_retry = 0
  syn_ack_retry = 0

  sim = None
  event_queue = None

  """help other variant to collect statistics"""
  stat = None

  def __init__(self, src_port, src_ip, dst_ip, recv_callback, \
               send_callback, sim, RTO = 2e5):
    self.RTO = RTO
    self.INIT_RTO = RTO

    self.src_port = src_port
    self.src_ip = src_ip
    self.dst_ip = dst_ip
    self.recv_callback = recv_callback
    self.send_callback = send_callback
    self.cur_window_size = self.MIN_WINDOW_SIZE
    self.remain_window_size = self.MIN_WINDOW_SIZE
    self.remain_pkts = Queue()
    self.window_pkts = Queue()
    #self.seq_no = -1
    self.send_seq_no = -1
    self.recv_seq_no = -1
    self.data_buf_size = 0
    self.ack_trigger_cnt = self.MIN_WINDOW_SIZE

    self.is_resending = False
    self.resend_cursor = 0

    self.is_syned = False
    self.scheduled_resend = False
    self.scheduled_timeout = False
    self.should_timeout = True
    self.is_closed= False
    self.fin_retry = 0
    self.syn_ack_retry = 0

    self.sim = sim
    self.event_queue = sim.get_event_queue()
    self.stat = {} 

  
  def create_packet(self, t, src_port, dst_port = -1, size = 1500, \
                    src_ip = '0.0.0.0', dst_ip = '1.1.1.1', \
                    seq_no = 0, action = None, proto = 'TCP'):
    return Packet(t, src_port, dst_port = dst_port, size = size, \
                  src_ip = src_ip, dst_ip = dst_ip, \
                  seq_no = seq_no, action = action, proto = 'TCP')

  """wrapper of send_callback, allow variants to modify the packet"""
  def _send_callback(self, t, data):
    (pkt, resend) = data
    pkt.created_time = t
    self.send_callback(t, pkt)
    if resend:
      #print "scheduled resend ", pkt
      self._schedule_resend(t)
      if not self.is_resending:
        #print "appending packet ", self.window_pkts.qsize(), pkt 
        self._window_append(pkt)

  def get_port(self):
    return self.src_port

  def _schedule_one_packet(self, t, pkt, resend):
    event = Event(time = t, callback = self._send_callback, \
                  data = (pkt, resend))
    self.event_queue.put((event.time, event))

  def _schedule_resend(self, t):
    if not self.scheduled_resend:
      self.scheduled_resend = True
      resend_time = t + self.RTO
      event = Event(time = resend_time, callback = self._trigger_resend)
      self.event_queue.put((resend_time, event))

  def _schedule_timeout(self, t):
    if not self.scheduled_timeout:
      self.schedule_timeout = True
      timeout_time = t + self.TIMEOUT
      event = Event(time = timeout_time, callback = self._timeout)
      self.event_queue.put((timeout_time, event))

  def _timeout(self, t, data = None):
    if self.should_timeout:
      #print "timeout", t, self.src_ip, src.src_port
      self._discard_all(t)
      self._report_fin(t)
    else:
      self.should_timeout = True
      self._schedule_timeout(t)

  def _window_append(self, pkt):
    #self.send_seq_no += 1
    self.window_pkts.put(pkt)

  def syn(self, t):
    assert False == self.is_closed

    self.send_seq_no += 1
    packet = self.create_packet(t, self.src_port, \
                                size = self.HEADER_SIZE, \
                                src_ip = self.src_ip, \
                                dst_ip = self.dst_ip, \
                                seq_no = self.send_seq_no, \
                                action = "SYN")

    self._send_callback(t, (packet, True))

  def send(self, t, data_size):
    assert False == self.is_closed

    pkt_num = math.ceil(data_size / (self.MTU_SIZE - self.HEADER_SIZE))

    if pkt_num > 0:
      self.remain_pkts.put([pkt_num, self.MTU_SIZE])

    remain_bytes = data_size % (self.MTU_SIZE - self.HEADER_SIZE)
    if remain_bytes > 0:
      self.remain_pkts.put([1, remain_bytes + self.HEADER_SIZE])

    """flow finish"""
    self.remain_pkts.put([-1])

    self._send_packets(t)

  def _send_packets(self, t):
    assert False == self.is_closed

    if not self.is_syned:
      return self.syn(t)
    if self.is_resending:
      return self._resend_packets(t)

    if self.remain_window_size <= 0 or self.remain_pkts.empty():
      return

    while self.remain_window_size > 0 and not self.remain_pkts.empty():
      pkt_size = self.remain_pkts.queue[0][1]

      self.send_seq_no += 1
      
      packet = self.create_packet(t, self.src_port, \
                                  dst_port = self.dst_port, \
                                  size = pkt_size, src_ip = self.src_ip, \
                                  dst_ip = self.dst_ip, \
                                  seq_no = self.send_seq_no, \
                                  action = "DAT")

      self.sim.stat['record_eff'](t, packet)

      self.remain_window_size -= 1
      self.remain_pkts.queue[0][0] -= 1
      if self.remain_pkts.queue[0][0] == 0:
        self.remain_pkts.get()

      if self.remain_pkts.queue[0][0] < 0:
        packet.action = "END"
        #self.seq_no = 0
        self.remain_pkts.get()

      """make sure they all have different key"""
      t += 1

      self._schedule_one_packet(t, packet, True)

  def _trigger_resend(self, t, data = None):
    if self.is_closed:
      return

    self.scheduled_resend = False

    if self.window_pkts.empty():
      return
    if t - self.window_pkts.queue[0].created_time < self.RTO - self.EPS:
      self._schedule_resend(self.window_pkts.queue[0].created_time)
    else:
      self.is_resending = True
      #print "cursor set to 0", self.window_pkts.queue[0]
      self.resend_cursor = 0
      self.cur_window_size = max(self.cur_window_size >> 1, self.MIN_WINDOW_SIZE)
      self.remain_window_size = self.cur_window_size
      self.RTO = min(self.RTO * 2, self.MAX_RTO)
      self._resend_packets(t)

  def _resend_packets(self, t, data = None):
    assert True == self.is_resending

    while self.remain_window_size > 0 and \
          self.resend_cursor < self.window_pkts.qsize():
      """make keys different"""
      t += 1

      packet = self.window_pkts.queue[self.resend_cursor]
      if "FIN" == packet.action:
        self.fin_retry += 1
        if self.fin_retry >= self.MAX_RETRY:
          self.is_closed = True
          self._report_fin(t)
          return
          
      if "SYN_ACK" == packet.action:
        self.syn_ack_retry += 1
        if self.syn_ack_retry >= self.MAX_RETRY:
          self.is_closed = True
          self._report_fin(t)
          return

      #print t, "setting resend field", self.resend_cursor, self.remain_window_size, self.window_pkts.qsize(), packet
      packet.resend = True
      self._send_callback(t, (packet, True))
      self.resend_cursor += 1
      self.remain_window_size -= 1
      #print t, "after resend field", self.resend_cursor, self.remain_window_size, self.window_pkts.qsize(), packet

    if self.window_pkts.empty():
      self.is_resending = False
      self._send_packets(t)
    
  def _confirm_one_packet(self, pkt):
    self.window_pkts.get()
    if self.cur_window_size < self.MAX_WINDOW_SIZE:
      self.cur_window_size += 1
      self.RTO = self.INIT_RTO

    self.remain_window_size += 2
    self.remain_window_size = min(self.remain_window_size, \
                                  self.cur_window_size)

    if self.is_resending:
      self.resend_cursor -= 1
      #print "dec cursor", pkt
    
  def _discard_all(self):
    while not self.window_pkts.empty():
      self.window_pkts.get()
    while not self.remain_pkts.empty():
      self.remain_pkts.get()

  def _handle_ack(self, t, pkt):
    """only occurs as the ack for SYN_ACK"""
    if 0 >= pkt.seq_no:
      return
    while not self.window_pkts.empty() and \
          self.window_pkts.queue[0].seq_no < pkt.seq_no:
      self._confirm_one_packet(pkt)
    self._send_packets(t)


  def _stat_packet(self, t, pkt):
    pass

  def recv(self, t, pkt):
    if self.is_closed:
      return

    self._stat_packet(t, pkt)

    self.should_timeout = False

    t += 1

    if "DAT" == pkt.action and pkt.seq_no <= self.recv_seq_no + 1:
      self.recv_seq_no = max(self.recv_seq_no, pkt.seq_no)
      self.data_buf_size += (pkt.size - self.HEADER_SIZE)
      self.ack_trigger_cnt -= 1

      if self.ack_trigger_cnt <= 0:
        """send ack"""
        self.ack_trigger_cnt = self.ACK_TRIGGER_MAX
        packet = self.create_packet(t, self.src_port, \
                                    dst_port = self.dst_port, \
                                    size = self.HEADER_SIZE, \
                                    src_ip = pkt.dst_ip, \
                                    dst_ip = pkt.src_ip, \
                                    seq_no = pkt.seq_no + 1, \
                                    action = "ACK")
        self._send_callback(t, (packet, False))

    elif "ACK" == pkt.action:
      self._handle_ack(t, pkt)

    elif "END" == pkt.action and pkt.seq_no <= self.recv_seq_no + 1:
      """deliver data to application"""
      if pkt.seq_no == self.recv_seq_no + 1:
        self.recv_seq_no = max(self.recv_seq_no, pkt.seq_no)
        self.data_buf_size += (pkt.size - self.HEADER_SIZE)
        self.recv_callback(t, (self, self.data_buf_size))
        self.data_buf_size = 0
        self.ack_trigger_cnt = self.ACK_TRIGGER_MAX

      """send END_ACK"""
      packet = self.create_packet(t, self.src_port, \
                                  dst_port = self.dst_port, \
                                  size = self.HEADER_SIZE, \
                                  src_ip = pkt.dst_ip, \
                                  dst_ip = pkt.src_ip, \
                                  seq_no = pkt.seq_no, \
                                  action = "END_ACK")
      self._send_callback(t, (packet, False))

    elif "END_ACK" == pkt.action:
      while not self.window_pkts.empty() and \
            not "END" == self.window_pkts.queue[0].action:
        self._confirm_one_packet(pkt)
      
      if not self.window_pkts.empty():
        """when RTO is small, the web may resend END even if END_ACK is on the way"""
        self._confirm_one_packet(pkt)

    elif "FIN" == pkt.action and pkt.seq_no <= self.recv_seq_no + 1:
      self.recv_seq_no = max(self.recv_seq_no, pkt.seq_no)
      self._discard_all()

      """send FIN_ACK"""
      packet = self.create_packet(t, self.src_port, \
                                  dst_port = self.dst_port, \
                                  size = self.HEADER_SIZE, \
                                  src_ip = pkt.dst_ip, \
                                  dst_ip = pkt.src_ip, \
                                  seq_no = pkt.seq_no, \
                                  action = "FIN_ACK")
      self._send_callback(t, (packet, False))

      self.is_closed = True
      self._report_fin(t)

    elif "FIN_ACK" == pkt.action:
      self._discard_all()
      self.is_closed = True
      
      self._report_fin(t)
    
    elif "SYN" == pkt.action and pkt.seq_no <= self.recv_seq_no + 1:
      self.recv_seq_no = max(self.recv_seq_no, pkt.seq_no)
      self.is_syned = True
      self.dst_port = pkt.src_port
      packet = self.create_packet(t, self.src_port, \
                                  dst_port = self.dst_port, \
                                  size = self.HEADER_SIZE, \
                                  src_ip = self.src_ip, \
                                  dst_ip = self.dst_ip, seq_no = 0, \
                                  action = "SYN_ACK")
      self._send_callback(t, (packet, False))

    elif "SYN_ACK" == pkt.action:
      """
        if the retry time is too short, web may send two SYN, which forces the 
        mem to create two ports
      """
      if self.dst_port > 0 and not self.dst_port == pkt.src_port:
        return

      self.window_pkts.get()
      self.dst_port = pkt.src_port
      packet = self.create_packet(t, self.src_port, \
                                  dst_port = self.dst_port, \
                                  size = self.HEADER_SIZE, \
                                  src_ip = pkt.dst_ip, \
                                  dst_ip = pkt.src_ip, seq_no = 0, \
                                  action = "ACK")
      self._send_callback(t, (packet, False))
  
      self.is_syned = True
      self._send_packets(t)

    else:
      #print("unrecgonized packet type or out-of-ordered: %s"%(pkt))
      return
   
  def _report_fin(self, t):
    self.recv_callback(t, (self, -self.src_port))

  def close(self, t):
    if self.is_closed:
      return
    self._discard_all()
    self.is_resending = False
    t += 1
    packet = self.create_packet(t, self.src_port, \
                                dst_port = self.dst_port, \
                                size = self.HEADER_SIZE, \
                                src_ip = self.src_ip, \
                                dst_ip = self.dst_ip, \
                                seq_no = self.send_seq_no + 1, \
                                action = "FIN")

    self._schedule_one_packet(t, packet, True)
