from server import Server
from event import Event

from simulator import Simulator
from test_tcp import TEST_TCP

from Queue import PriorityQueue
from sets import Set
import random
import math


"""
server.recv -> tcp.recv -> app.request -> app.get/set -> tcp.send -> server.send
"""
class MemServer(Server):

  BANDWIDTH = 2e8 / 8.0
  MEM_SPEED = 4 * 800

  out_wait = 0
  bw = 0

  neighbor = None

  def __init__(self, id, ip, sim, RTO = 2e5, proto = 'TCP'):
    Server.__init__(self, id, ip, sim, RTO, proto)
    self.service_ips = Set()
    self.out_wait = 0
    self.bw = self.BANDWIDTH
    self.neighbor = None
    
  def get_max_bw(self):
    return self.BANDWIDTH


  def _calc_mem_latency(self, size):
    return math.ceil(size / self.MEM_SPEED)

  def _random_response_size(self):
    return 1e4

  def listen(self, t, pkt):
    tcp = self.create_proto(self._get_port(), pkt.dst_ip, \
                            pkt.src_ip, self.request, \
                            self.nic_buff, self.sim, self.RTO)
    self.conn_pool[tcp.src_port] = tcp    
    tcp.recv(t, pkt)

  def request(self, t, data):
    """use fixed payload_size for now"""
    (tcp, request_size) = data

    if request_size < 0:
      tcp.close(t)
      self.conn_pool.pop(tcp.src_port)
    else:
      response_size = self._random_response_size()
      t += self._calc_mem_latency(response_size)
      event = Event(time = t, callback = tcp.send, data = response_size)
      self.event_queue.put((t, event))

  def get_bw_util(self):
    return self.get_bw_recv() / float(self.bw)

  """
    recv and send are pkt level apis. application level should
    call TCP apis
  """
  def nic_recv(self, t, pkt):
    #print 'recv', t, '\t', pkt
    self._stat_nic_recv(t, pkt)
    t += self.OS_LATENCY
    event = None
      
    if pkt.dst_port < 0:
      event = Event(time = t, callback = self.listen, data = pkt)
    elif pkt.dst_port in self.conn_pool.keys():
      tcp = self.conn_pool[pkt.dst_port]
      event = Event(time = t, callback = tcp.recv, data = pkt)
    else:
      return
      
    self.event_queue.put((t, event))


  def nic_buff(self, t, pkt):
    if not "SYN" == pkt.action:
      self.pkts_buff += 1
    """self.bw already is the min of self.bw and neighbor.bw"""
    transmit_latency = 1e6 * pkt.size / self.bw
    self.out_wait = max(t, self.out_wait) + transmit_latency

    t = self.out_wait + self.OS_LATENCY
    event = Event(time = t, callback = self.nic_send, data = pkt)
    self.event_queue.put((t, event))

  def nic_send(self, t, pkt):
    pkt.ack_weight = max(0, 1 - self.get_bw_util())
    if self.sim.debug:
      print t, '\t', pkt
    self.sim.stat['record_all'](t, pkt)
    if not "SYN" == pkt.action:
      self.pkts_sent += 1

    self.neighbor.nic_recv(t, pkt)

  def get_fib(self, except_ports = []):
    return [self.ip]

  def insert_fib(self, port, ips):
    pass

  def connect(self, node):
    self.neighbor = node
    self.bw = min(self.BANDWIDTH, node.bw)


def main():
  sim = Simulator()
  tester = TEST_TCP("1.1.1.1", sim.get_event_queue())
  server = MemServer(0, "2.2.2.2", sim.get_event_queue())

  tester.connect(server)
  server.connect(tester)

  tester.app_send(0, 200)

  sim.simulate()

if __name__=="__main__":
  main()
