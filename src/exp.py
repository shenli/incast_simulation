from simulator import Simulator
from web_server import WebServer
from mem_server import MemServer
from switch import Switch

import math

class Exper:
  rtos = 0
  obj_num = 0
  proto = 0

  sim = None
  webs = None
  mems = None

  def __init__(self, RTO = 2e5, OBJ_NUM = 350, PROTO = 'TCP'):
    self.rto = RTO
    self.obj_num = OBJ_NUM
    self.proto = PROTO

    self.sim = None
    self.webs = []
    self.mems = []

  def configure(self):
    self.sim = Simulator()
    self.sim.debug = False
    switch1 = Switch(0, self.sim)
    switch2 = Switch(1, self.sim, port_num = self.obj_num + 200)

    self.webs.append(WebServer(100, '1.1.1.1', \
                               self.sim, self.rto, self.obj_num, \
                               proto = self.proto))

    switch1.connect(0, self.webs[0], -1)
    self.webs[0].connect(switch1)

    for i in range(2, 2 + self.obj_num + 100):
      ip = ".".join(['%d'%(i) for x in range(4)])
      mem = MemServer(100 + i, ip, self.sim, self.rto, proto = self.proto)
      self.mems.append(mem)
      switch2.connect(i, mem, -1)
      mem.connect(switch2)
      self.webs[0].register_service(mem.ip)

    switch1.connect(1, switch2, 1)
    switch2.connect(1, switch1, 1)

    self.webs[0].request_send(0)

  def run(self):
    self.configure()
    self.sim.simulate()
    print self.webs[0].req_2_ports
    d = sum(self.webs[0].get_delays()) / float(len(self.webs[0].get_delays()))
    p = 0

    for mem in self.mems:
      p += mem.get_pkts_sent()
    
    for web in self.webs:
      p += web.get_pkts_sent()

    return (d, p)


def main():
  exp_num = 5
  rtos = [5e2, 1e3, 2e3, 5e3, 10e3, 50e3, 100e3, 200e3]
  objs = [50, 100, 150, 200, 250, 300, 350]
  protos = ['TCP', 'LPTCP', 'ICTCP']
  #objs = [350]
  rtos = [1e4]
  protos = ['TCP', 'LPTCP', 'ICTCP', 'JIFFY', 'RTOMin', 'JRTOMin']


  fout = open('../data/all.out', 'w')

  cnt = 0
  for obj in objs:
    for rto in rtos:
      cnt += 1
      for proto in protos:
        for i in range(exp_num):
          one_out = open('../data/%s_%d_%d_%d'%(proto, obj, rto, i), 'w')
          exper = Exper(RTO = rto, OBJ_NUM = obj, PROTO = proto)
          (delay, pkts) = exper.run()
          str_out = "%d,%d,%d,%d,%d,%s"%(cnt, obj, rto, delay, pkts, proto)
          print str_out
          fout.write("%s\n"%(str_out))
          exper.sim.write(one_out)
          one_out.close()
  
  fout.close()


if __name__=="__main__":
  main()
