from Queue import Queue, PriorityQueue
from tcp import TCP
from lp_packet import Packet
import math
import random

from event import Event

class JIFFY(TCP):

  def __init__(self, src_port, src_ip, dst_ip, recv_callback, \
               send_callback, sim, RTO = 2e5):
    TCP.__init__(self, src_port, src_ip, dst_ip, recv_callback, \
                 send_callback, sim, RTO)
    
    self.RTO = self.RTO * (0.5 + random.random())
