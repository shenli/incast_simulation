from packet import Packet

class LpPacket(Packet):
  
  lp_delay = 0
  ack_weight = 1.0

  def __init__(self, created_time, src_port, dst_port = -1, \
               size = 1500, src_ip = '0.0.0.0', dst_ip = '1.1.1.1', \
               seq_no = 0, action = None, proto = "LPTCP", \
               lp_delay = 0, ack_weight = 1.0):
    Packet.__init__(self, created_time, src_port, dst_port, \
               size, src_ip, dst_ip, seq_no, action, proto)
    assert "LPTCP" == proto                                                           
    self.lp_delay = lp_delay;
    self.ack_weight = ack_weight


  def __str__(self):
    lp_str = "delay = %d, weight = %.3f"%(self.lp_delay, self.ack_weight)
    return "%d,%s,%s"%(self.created_time, Packet.__str__(self), lp_str)
