from sets import Set
from Queue import Queue
import math

from node import Node
from tcp import TCP
from lptcp import LPTCP
from ictcp import ICTCP
from jiffy import JIFFY
from rtomin import RTOMin
from jrtomin import JRTOMin

class Server(Node):
  RTO = 0

  port_pool = None
  port_cursor = 1024
  conn_pool = None

  ip = None

  pkts_buff = 0
  pkts_sent = 0
  proto = None

  nic_stat = 0.0
  bw_data = None
  bw_dur = 1000.0
  bw_sum = 0.0
  bw = 1

  def __init__(self, id, ip, sim, RTO, proto = 'TCP'):
    Node.__init__(self, id, sim)
    self.ip = ip
    self.conn_pool = {}
    self.port_pool = Set()
    self.port_cursor = 1024
    self.pkts_sent = 0
    self.pkts_buff = 0
    self.RTO = RTO
    self.proto = proto

    self.nic_stat = {}
    self.nic_stat['bw_recv'] = 0.0
    self.nic_stat['recv_wait'] = 0.0
    self.bw_data = Queue()
    self.bw_dur = 1000.0
    self.bw_sum = 0.0

  def _get_port(self):
    if len(self.port_pool) > 0:
      return self.port_pool.pop()
    else:
      port = self.port_cursor
      self.port_cursor += 1
      return port


  def get_pkts_buff(self):
    return self.pkts_buff

  def get_pkts_sent(self):
    return self.pkts_sent

  def get_queue_len(self):
    return self.pkts_buff - self.pkts_sent

  def get_wait_time(self):
    w = 1e6 * 1e3 / self.bw
    send_wait = math.floor(self.get_queue_len() * w)
    recv_wait = self.nic_stat['recv_wait']

    return max(send_wait, recv_wait)

  def get_bw_recv(self):
    return self.nic_stat['bw_recv']

  def get_bw_util(self):
    print("calling abstract get_bw_util")
    exit(0)

  def _stat_nic_recv(self, t, pkt):
    lb = t - self.bw_dur
    while not self.bw_data.empty() and \
          self.bw_data.queue[0][0] < lb:
      (pt, psize) = self.bw_data.get();
      self.bw_sum -= psize

    self.bw_data.put((t, pkt.size));
    self.bw_sum += pkt.size
    self.nic_stat['bw_recv'] = 1e6 * self.bw_sum / self.bw_dur

    if self.nic_stat['bw_recv'] / self.bw > 0.95:
      self.nic_stat['recv_wait'] += 20
    else:
      self.nic_stat['recv_wait'] = max(0, self.nic_stat['recv_wait'] - 30)
      

  def create_proto(self, src_port, src_ip, dst_ip, recv_callback, \
                   send_callback, event_queue, RTO):
    if 'TCP' == self.proto:
      return TCP(src_port, src_ip, dst_ip, recv_callback, \
                 send_callback, event_queue, RTO)
    elif 'LPTCP' == self.proto:
      return LPTCP(src_port, src_ip, dst_ip, recv_callback, \
                   send_callback, event_queue, self.get_wait_time,\
                   self.get_bw_util, RTO)
    elif 'ICTCP' == self.proto:
      return ICTCP(src_port, src_ip, dst_ip, recv_callback, \
                   send_callback, event_queue, RTO)
    elif 'JIFFY' == self.proto:
      return JIFFY(src_port, src_ip, dst_ip, recv_callback, \
                   send_callback, event_queue, RTO)
    elif 'RTOMin' == self.proto:
      return RTOMin(src_port, src_ip, dst_ip, recv_callback, \
                   send_callback, event_queue, RTO)
    elif 'JRTOMin' == self.proto:
      return JRTOMin(src_port, src_ip, dst_ip, recv_callback, \
                   send_callback, event_queue, RTO)





