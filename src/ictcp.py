from Queue import Queue, PriorityQueue
from tcp import TCP
from lp_packet import Packet
import math

from event import Event

class ICTCP(TCP):

  def __init__(self, src_port, src_ip, dst_ip, recv_callback, \
               send_callback, sim, RTO = 2e5):
    TCP.__init__(self, src_port, src_ip, dst_ip, recv_callback, \
                 send_callback, sim, RTO)
    
    self.MIN_WINDOW_SIZE = 1
    self.MAX_WINDOW_SIZE = 1
    self.INIT_WINDOW_SIZE = 1
