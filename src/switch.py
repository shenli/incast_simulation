from simulator import Simulator
from mem_server import MemServer
from web_server import WebServer

from node import Node
from event import Event

"""
1. Time units is micro-seconds.
2. NetGear GS108 buffers at most 192KB data, and maximum 32 KB per port
3. The max switch latency is 6 micro-seconds for 100Mb link
4. data is buffered at out-going link
5. switch follows receive->process->send steps. receive only adds 6 micro-second 
    latency.
"""
class Switch(Node):
  BANDWIDTH = 1e9 / 8.0
  SWITCH_LATENCY = 6
  MAX_BUF = 192000
  PORT_BUF = 32000
  PORT_NUM = 8

  remain_buf = 0
  bw = None

  event_queue = None;

  port_buf_list = None
  port_out_wait = None;
  neighbors = None;
  fib = None

  def __init__(self, id, sim, port_num = -1):
    Node.__init__(self, id, sim)

    """for testing"""
    if port_num > 0:
      self.PORT_NUM = port_num

    self.remain_buf = self.MAX_BUF
    self.bw = [self.BANDWIDTH for x in range(self.PORT_NUM)]

    self.port_buf_list = [self.PORT_BUF for i in range(self.PORT_NUM)]
    self.port_out_wait = [0 for i in range(self.PORT_NUM)]
    self.neighbors = [None for i in range(self.PORT_NUM)]
    self.fib = {}

  def get_max_bw(self):
    return self.BANDWIDTH


  """
    since the fib is not what we want to optimize, we are using
    complete ip instead of ip prefix here
  """
  def insert_fib(self, port, ips):
    for ip in ips:
      self.fib[ip] = port

    for i in range(self.PORT_NUM):
      if self.neighbors[i] is not None:
        self.neighbors[i][0].insert_fib(i, ips)

  def get_fib(self, except_ports = []):
    ips = []
    for ip in self.fib.keys():
      if self.fib[ip] not in except_ports:
        ips.append(ip)

    return ips

  def connect(self, port, node, node_port):
    ips = node.get_fib(except_ports = [node_port])
    self.insert_fib(port, ips)

    self.neighbors[port] = (node, node_port)
    self.bw[port] = min(self.BANDWIDTH, node.get_max_bw())

  def nic_recv(self, t, pkt):
    event = Event(time = t + self.SWITCH_LATENCY, callback = self.nic_buff, data = pkt)
    self.event_queue.put((event.time, event))
    return [self.id]

  def nic_buff(self, t, pkt):
    if not pkt.dst_ip in self.fib.keys():
      print("cannot resolve ip %s"%(pkt.dst_ip))
      exit(0)

    dst_port = self.fib[pkt.dst_ip]
    if self.remain_buf < pkt.size or \
      self.port_buf_list[dst_port] < pkt.size:
      if self.sim.debug:
        print("=============== %d pkt dropped: %s"%(t, pkt))
      self.sim.stat['record_drop'](t, pkt)
      """pkt dropped"""
      return [self.id]

    self.remain_buf -= pkt.size
    self.port_buf_list[dst_port] -= pkt.size

    """1s = 1e6 micro-s"""
    transmit_latency = 1e6 * pkt.size / self.bw[dst_port]
    self.port_out_wait[dst_port] = \
                 max(t, self.port_out_wait[dst_port]) + transmit_latency

    event = Event(time = self.port_out_wait[dst_port], \
                  callback = self.nic_send, data = pkt)
    self.event_queue.put((event.time, event))  
  
    return [self.id]


  def nic_send(self, t, pkt):
    #print self.id, t, '\t', pkt
    dst_port = self.fib[pkt.dst_ip]
  
    self.remain_buf += pkt.size
    self.port_buf_list[dst_port] += pkt.size

    self.neighbors[dst_port][0].nic_recv(t, pkt)

    return [self.neighbors[dst_port][0].id]


def main():
  sim = Simulator()
  switch1 = Switch(0, sim.get_event_queue())
  switch2 = Switch(1, sim.get_event_queue())

  web = WebServer(100, '1.1.1.1', sim.get_event_queue())
  mem = MemServer(101, '2.2.2.2', sim.get_event_queue())

  switch1.connect(0, web, -1)
  web.connect(switch1)

  switch2.connect(0, mem, -1)
  mem.connect(switch2)


  switch1.connect(1, switch2, 1)
  switch2.connect(1, switch1, 1)

  web.register_service(mem.ip)
  web.OBJ_NUM = 1 
  web.request_send(0)

  sim.simulate()


if __name__=="__main__":
  main()
