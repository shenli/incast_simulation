from tcp import TCP
from sets import Set
from Queue import PriorityQueue
from event import Event
from simulator import Simulator

class TEST_TCP:

  peer = None
  event_queue = None
  os_delay = None
  ip = None
  coon_pool = None
  port_pool = None
  port_cursor = 1024

  bw = 0

  def __init__(self, ip, event_queue):
    self.ip = ip
    self.event_queue = event_queue
    self.os_delay = 10
    self.conn_pool = {}
    self.port_pool = Set()
    self.port_cursor = 1024

  def _get_port(self):
    if len(self.port_pool) > 0:
      return self.port_pool.pop()
    else:
      port = self.port_cursor
      self.port_cursor += 1
      return port


  def connect(self, peer):
    self.peer = peer
    self.bw = peer.bw

  def app_recv(self, t, data):
    (tcp, data_size) = data
    print("app recv at time %d, %d bytes data"%(t, data_size))

    if data_size > 0:
      tcp.close(t)
    else:
      self.port_pool.add(-data_size)
      self.conn_pool[-data_size].close(t)
      self.conn_pool.pop(-data_size)

      


  def app_send(self, t, data_size):
    tcp = TCP(self._get_port(), self.ip, self.peer.ip, self.app_recv, \
              self.nic_send, self.event_queue)
    self.conn_pool[tcp.get_port()] = tcp
    tcp.send(t, data_size)

  def nic_send(self, t, pkt):
    print t, '\t',  pkt
    t += self.os_delay
    event = Event(time = t, callback = self.peer.nic_recv, data = pkt)
    self.event_queue.put((t, event))

  def nic_recv(self, t, pkt):
    if pkt.dst_port >= 0:
      if pkt.dst_port not in self.conn_pool.keys():
        print t, pkt
      self.conn_pool[pkt.dst_port].recv(t, pkt)
    else:
      tcp = TCP(self._get_port(), pkt.dst_ip, pkt.src_ip, \
                self.app_recv, self.nic_send, self.event_queue)
      self.conn_pool[tcp.get_port()] = tcp
      event = Event(time = t, callback = tcp.recv, data = pkt)
      self.event_queue.put((t, event))

def main():
  sim = Simulator()
  AA = TEST_TCP("1.1.1.1", sim.get_event_queue())
  BB = TEST_TCP("2.2.2.2", sim.get_event_queue())

  AA.connect(BB)
  BB.connect(AA)

  AA.app_send(0, 9678)
  sim.simulate()

if __name__=="__main__":
  main()
