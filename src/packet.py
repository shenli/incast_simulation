class Packet:
  proto = None
  size = 0
  src_ip = None
  dst_ip = None
  src_port = 0
  dst_port = 0
  seq_no = 0
  action = None

  created_time = 0
  resend = False

  def __init__(self, created_time, src_port, dst_port = -1, size = 1500, src_ip = '0.0.0.0', \
               dst_ip = '1.1.1.1', seq_no = 0, action = None, proto = 'TCP'):
    self.created_time = created_time
    self.size = size;
    self.src_port = src_port
    self.dst_port = dst_port
    self.src_ip = src_ip
    self.dst_ip = dst_ip
    self.seq_no = seq_no
    self.action = action
    self.proto = proto
    self.resend = False

  def __str__(self):
    str1 = "size = %d, src_port = %d, dst_port = %d, "%(self.size, \
                                      self.src_port, self.dst_port)
    str2 = "src = %s, dst = %s, seq = %d, action = %s"%(self.src_ip, \
                             self.dst_ip, self.seq_no, self.action)
    return "%s%s, resend = %d, proto = %s"%(str1, str2, self.resend, \
                                            self.proto)
