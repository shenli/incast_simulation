class Event:
  time = 0
  callback = None
  data = None

  def __init__(self, time = 0, callback = None, data = None):
    self.time = time;
    self.callback = callback;
    self.data = data

  def execute(self):
    self.callback(self.time, self.data)
