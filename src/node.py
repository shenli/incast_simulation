from sets import Set

"""
base class for both switch and server
"""
class Node:

  INFTY = 1e30
  OS_LATENCY = 10

  id = 0
  sim = None
  event_queue = None

  def __init__(self, id, sim):
    self.id = id
    self.event_queue = sim.get_event_queue()
    self.sim = sim

  def get_next_event_time(self):
    print "calling abstract get_next_event_time!!!"
    exit(0)

  def do_next_evet(self):
    print "calling abstract do_next_event!!!"
    exit(0)


  def get_fib(self, except_ports = []):
    print "calling abstract get_fib!!!"
    exit(0)

  def insert_fib(self, ips):
    print "calling abstract insert_fib!!!"
    exit(0)
