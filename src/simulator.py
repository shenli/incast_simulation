from Queue import PriorityQueue

class Simulator:

  event_queue = None
  stat = None

  t_slot = 5000

  key_2_index = None

  def __init__(self):
    self.event_queue = PriorityQueue()
    self.stat = {}
    self.stat['trace'] = [[0, 0, 0, 0]]
    self.stat['record_all'] = self.record_all
    self.stat['record_eff'] = self.record_eff
    self.stat['record_drop'] = self.record_drop

    self.key_2_index = {}
    self.key_2_index['all'] = 1
    self.key_2_index['eff'] = 2
    self.key_2_index['drop'] = 3

  def record_template(self, prefix, t, pkt):
    prev_time = self.stat['trace'][-1][0]
    index = self.key_2_index[prefix]
    if t - prev_time > self.t_slot:
      self.stat['trace'].append([prev_time + self.t_slot, 0, 0, 0])
      self.stat['trace'][-1][index] += pkt.size
    else:
      self.stat['trace'][-1][index] += pkt.size

  def record_eff(self, t, pkt):
    self.record_template('eff', t, pkt)

  def record_all(self, t, pkt):
    self.record_template('all', t, pkt)

  def record_drop(self, t, pkt):
    self.record_template('drop', t, pkt)

  def do_next_event(self):
    if self.event_queue.empty():
      return -1

    else:
      (t, event) = self.event_queue.get()
      event.execute()
      return t


  def get_event_queue(self):
    return self.event_queue


  def simulate(self):
    while not self.event_queue.empty():
      self.do_next_event()

  def write(self, out):
    for i in range(len(self.stat['trace'])):
      out.write('%d, %d, %d, %d\n'%(self.stat['trace'][i][0],
                                  self.stat['trace'][i][1],
                                  self.stat['trace'][i][2],
                                  self.stat['trace'][i][3]))
